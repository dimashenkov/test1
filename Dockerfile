FROM alpine:3.9

RUN apk --update add jq curl wget libc6-compat gettext bash && \
  rm -rf /var/cache/apk/* && \
  addgroup -S cicd && \
  adduser -D -S cicd -s /bin/sh -h /cicd && \
  wget --no-verbose -O /usr/local/bin/cmk.bin https://github.com/apache/cloudstack-cloudmonkey/releases/download/6.0.0/cmk.linux.x86-64 && \
  chmod +x /usr/local/bin/cmk.bin && \
  mkdir /cicd/.cmk && \
  chown cicd:cicd /cicd/.cmk


USER cicd
WORKDIR /cicd 

